import sys
import math

document1 = 'doc1.txt'
document2 = 'doc2.txt'
document3 = 'doc3.txt'

def tf_mul_idf(tf_list, idf_dictionary):
    tf_mul_dict = {}
    rownum = 0
    for tf_dic in tf_list:
        for term in tf_dic:
	    tf_update = 0.0
	    if term in idf_dictionary:
	       tf_update = tf_dic[term] * idf_dictionary[term]

	    if term in tf_mul_dict:
	       tf_mul_dict[term].append([rownum, tf_update])
	    else:
	       tf_mul_dict[term] = [[rownum, tf_update]]

            #print term, tf_mul_dict[term], tf_update, rownum
        rownum += 1

    print '\nTF * IDF:\n', tf_mul_dict
    return tf_mul_dict

def normalize_term_frequency(term_frequency):
    sum = 0.0
    norm_term_frequency = {}
    for key in term_frequency:
        sum += term_frequency[key]
	#print float(term_frequency[key])

    #print "SUM:", sum

    for key in term_frequency:
        norm_term_frequency[key] = float(term_frequency[key]) / float(sum)

    return norm_term_frequency

def inverse_doc_freq (dictionary_list):
    numberOfDocs = len(dictionary_list)
    docNum = 0
    term_cross_doc_list = []
    #print 'TERM_COUNT', dictionary_list

    for word_dictionary in dictionary_list:
        #print 'Document #', docNum, ':', word_dictionary
	cross_doc_count = {}
	for term in dictionary_list[docNum]:
            for word_dictionary2 in dictionary_list:
	        if term in word_dictionary2.keys():
		   #print term, 'present in', docNum, ':', word_dictionary2.keys()
		   if term in cross_doc_count:
		      cross_doc_count[term] += 1
		   else:
		      cross_doc_count[term] = 1
	term_cross_doc_list.insert(docNum, cross_doc_count)
        docNum += 1

    docNum = 0

    inv_term_freq = {}
    for word_dictionary in dictionary_list:
        for term in dictionary_list[docNum]:
	    if term in term_cross_doc_list[docNum]:
	       inv_term_freq[term] = 1.0 + math.log(float(numberOfDocs) / float(term_cross_doc_list[docNum][term]))

	docNum += 1

    #print 'UPDATED_TERM_COUNT', dictionary_list
    #print 'TERM_CROSS_DOC_COUNT', term_cross_doc_list
    #print 'INV_TERM_FREQ', inv_term_freq

    return inv_term_freq


def gen_term_frequency(fname):
    word_dictionary = dict()
    fp = open(fname, 'r')
    row = 0

    #Read the file line by line
    for line in fp.readlines():
        row += 1
        #Get the words from a line as a list in words variable of type list
        #  rstrip() removes the newline character at the end of the line
        words = line.rstrip().lower().split()
        print 'Line#', row, ':', words

        #Save the words in the dictionary with count of each word
	for word in words:
	    if word in word_dictionary:
	       word_dictionary[word] += 1
	    else:
	       word_dictionary[word] = 1


    return word_dictionary

    fp.close()

def main_call():
    doc_list = []
    term_frequency_file1 = gen_term_frequency(document1)
    #print 'TERM:', term_frequency_file1

    norm_term_frequency_file1 = normalize_term_frequency(term_frequency_file1)
    #print 'NORM_TERM:', norm_term_frequency_file1

    term_frequency_file2 = gen_term_frequency(document2)
    norm_term_frequency_file2 = normalize_term_frequency(term_frequency_file2)

    term_frequency_file3 = gen_term_frequency(document3)
    norm_term_frequency_file3 = normalize_term_frequency(term_frequency_file3)

    doc_list = [term_frequency_file1, term_frequency_file2, term_frequency_file3]
    norm_doc_list = [norm_term_frequency_file1, norm_term_frequency_file2, norm_term_frequency_file3]

    inv_doc_freq = inverse_doc_freq(doc_list)

    print '\nNormalized Term Frequency (TF):\n', norm_doc_list
    print '\nInverse Document Frequency (IDF):\n', inv_doc_freq
    tf_mul_idf (norm_doc_list, inv_doc_freq)

main_call()
